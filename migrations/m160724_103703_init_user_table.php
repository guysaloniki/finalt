<?php

use yii\db\Migration;

class m160724_103703_init_user_table extends Migration
{
    public function up()
    {
		$this->createTable(
		'lead',
		[
		
		'id' => 'pk',
		'name' =>'string',
		'email' => 'string',
		'phone' => 'string',
		'notes' => 'text',
		'status' => 'integer',
		'owner' => 'integer',
		'created_at' => 'integer',
		'updated_at' => 'integer',
		'created_by' => 'integer',
		'updated_by' => 'integer',
		],
		'ENGINE=InnoDB'
		
		);

    }

    public function down()
    {
        echo "m160724_103703_init_user_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
